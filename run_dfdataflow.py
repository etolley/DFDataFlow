#=======
# Run as:
# python run_dfdataflow.py -i ipfiles/ttbar.NTUP_FTKIP_slim.root
#	
#======

import sys, getopt, time, os
import json

# imports from python directory
import modulereader as mr
import boardmap as bm
from dfconfigparser import parseModuleListFile
from outputhelper import OutputHelper
import logging
import utils

def main(argv):

	#===== Steering =====#
	inputfilename = ''
	outputfiledir = ''
	configfilename = '' 
	maxEvts = 0
	loglevel = "DEBUG"#"DEBUG" INFO
	board_enable_list = range(32) 
	deterministicMap = True
	doExtraChecks = False

	# read in command arguments
	try:
		opts, args = getopt.getopt(argv,'hi:c:o:n:l:r',['file='])
	except getopt.GetoptError:
		print "run_dfdataflow.py -c <configfilename> -i <inputfilename> -o <outputfiledir> -n <# events> -l <loglevel>"
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print "Usage: run_dfdataflow.py -c <configfilename> -i <inputfilename> -o <outputfiledir> -n <# events> -l <loglevel>"
			sys.exit()
		elif opt in ('-i', '--ifile'):
			inputfilename = arg
		elif opt in ('-o', '--ofile'):
			outputfiledir = arg
		elif opt in ('-c', '--cfile'):
			configfilename = arg
		elif opt in ('-n', '--nevents'):
			maxEvts = int(arg)
		elif opt in ('-l', '--log'):
			loglevel = arg
		elif opt == '-r':
			deterministicMap = False

	# set default config file if none was provided
	if configfilename == '':
		configfilename = 'config/default.config'
		print "No config file provided! Using default config file {0}".format(configfilename)
		print "Define a config file with: run_dfdataflow.py -c <configfilename>"

	extraouttag = ""
	
	# set variales from config file if variales were not defined in command line
	configfile = open(configfilename)
	for line in configfile:
		if line[0] == '#': continue
		toks = [t.strip() for t in line.split()]
		if len(toks) == 0: continue
		key, value = toks[0], toks[1]
		if key == "inputfilename" and inputfilename == '':
			inputfilename = value
		elif key == "nevents" and maxEvts == 0:
			maxEvts = int(value)
		elif key == "modulelist_configfile":
			modulelist_configfile = value
		elif key == "multiboard_configfile":
			multiboard_configfile = value
		elif key == "system_configfile":
			system_configfile = value
		elif key == "pixelmap":
			with open(value) as json_file:  
				pixelmap = json.load(json_file)
		elif key == "sctmap":
			with open(value) as json_file:  
				sctmap = json.load(json_file)
		elif key == "outtag":
			extraouttag = value

	print "Input file is %s"%inputfilename

	# set output file name template (will be part of all output file names)
	if outputfiledir == '':
		outputfiledir = "output/DataFlow_{0}_{1}evts".format( inputfilename.replace('/','.').split('.')[-2], maxEvts)
		outputfiledir += "_{0}boards{1}".format(len(board_enable_list), '_fixedMap' if deterministicMap else '_randomMap')
		if len(extraouttag) > 0: 
			outputfiledir += "_{0}".format(extraouttag)
		print "No custom output directory provided, will write out files to {0}".format(outputfiledir)


	if not os.path.exists(outputfiledir): os.makedirs(outputfiledir)

	numeric_level = getattr(logging, loglevel.upper(), None)
	if not isinstance(numeric_level, int):
	    raise ValueError('Invalid log level: %s' % opt)
	logging.basicConfig(level=numeric_level,
		                format='%(levelname)s: %(message)s',
		                filename="{0}/output.log".format(outputfiledir),
		                filemode='w')

	#===== Build DF map =====#
		
	# ROBmap: ROBId -> tower -> [ (modid, layer) ]
	(robid_modid_tower_map, robid_modid_plane_map) = parseModuleListFile( modulelist_configfile)

	#board_enable_list = [0,4,8,12,16,20,24,28,2,6,10,14,18,22,26,30]#range(33) # [2,14]  [2,30,15]#
	
	# trace this module
	trace = (1180489, 48, 0)
	trace = (2359307,834, 0)
	trace = ( 1311043, 303, 0)
	
	boardMap = bm.BoardMap(board_enable_list)
	boardMap.deterministicMap = deterministicMap
	if boardMap.deterministicMap:
		  utils.log("Picking 3-step links that minimize dataflow")
	else: utils.log("Randomly picking degenerate 3-step links")
	boardMap.setTraceModule(trace)

	boardMap.setBoards()
	boardMap.setConnections(system_configfile)
	boardMap.setROBIDs(multiboard_configfile)

	missing_boards = [b for b in range(32) if  b not in boardMap.boardDict.keys()]
	print "Missing boards: {0}".format(missing_boards)

	# set which boards want which robids
	boardMap.setDesiredInputs(robid_modid_tower_map, robid_modid_plane_map)
	 
	for r in ['0x121654']:#['0x130146', '0x112521', '0x24000e']:
		print 'Checking boards that want robid {0}'.format(r)
		for n, b in boardMap.boardDict.iteritems():
			if b.wantsROBID(r): print n

	#boardMap.boardDict[2].printDesiredROBIDs(utils.toHex(1311043))
	

	# now set all links between boards
	boardMap.setInterBoardLinks()
	# check missing links
	towers_missing_inputs = boardMap.towers_missing_inputs 
	print "Towers missing inputs:"
	for t, missing_inputs in towers_missing_inputs.iteritems():
		if len(missing_inputs) > 0:
			print "Tower {0} will not receive: {1}".format(t, [ (utils.toHex(r), "{0} modules".format(len(missing_inputs[r])))  for r in missing_inputs.keys()])
	print "Only doing data flow for towers {0}".format(boardMap.getActiveTowers())

	#sys.exit()

	boardMap.printSummary()
	boardMap.write(outputfiledir)

	sys.exit(2)

	myoutput = OutputHelper(outputfiledir)
	myoutput.setTraceModule(trace)

	# set up module reader
	moduleReader = mr.ModuleReader(inputfilename, pixelmap, sctmap, maxEvts = maxEvts)
	moduleReader.robid_modid_plane_map = robid_modid_plane_map
	moduleReader.robid_modid_tower_map = robid_modid_tower_map

	#===== Get input data and run data flow =====#
	# Process events & do data flow
	boardMap.printTiming = True
	for e in range(maxEvts):
		eventtimestart = time.time()
		timecheck = eventtimestart
		event = moduleReader.getEvent()
		print "##########################"
		print "# Processing Event {0}".format(event)
		print "##########################"
	
		# read event from input file
		moduleReader.readEvent()
		utils.printtime("Input file event read in {0} seconds".format(time.time()-timecheck))
		timecheck = time.time()
		
		boardMap.receiveIMModules(moduleReader.getModules())
		utils.printtime("Modules processed in {0} seconds".format(time.time()-eventtimestart))
		boardMap.doDataFlow(myoutput, event = event, printSummary = False, savePerEventOutput = False)
		utils.printtime("Event processed in {0} seconds".format(time.time()-eventtimestart))

	myoutput.writeModuleSummary(getOutputType = boardMap.getOutputType)
	myoutput.writeBoardSummary(boardMap.boardDict, maxEvts)

	desired_towers_out = [52, 4, 20, 36]
	desired_boards_out = [b.board_number for b in boardMap.boardDict.values() if b.tower1 in desired_towers_out or b.tower2 in desired_towers_out]
	for b in desired_boards_out:
		myoutput.writeOutLanes(b, getOutLane = boardMap.getSLinkOutChannel)

	print "done"


if __name__=='__main__':
		main(sys.argv[1:])