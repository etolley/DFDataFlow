# DF Data Flow

This code package has script to study FTK data flow in the DF system.

## Setup

Before running any code do:
```
source setup.sh
``` 

## Skimming & analyzing IP Files

Most of the code uses skimmed IP files as inputs. Some pre-existing files are stored in the `data` directory.
To make new files, run:

```
root -l make_slimmed_ip_file.cxx
```

You will need to edit the input and output files by hand in the cpde.

In one of the input IP files, the ftk hits tree contains [FTKRawHits](https://gitlab.cern.ch/atlas/athena/blob/21.3/Trigger/TrigFTK/TrigFTKSim/TrigFTKSim/FTKRawHit.h) organized by tower, ie:

 - branch: vector of FTKRawHits for Tower 0
 - branch: vector of FTKRawHits for Tower 1
 - etc

The output of `make_slimmed_ip_file.cxx` makes flat ntuples of the module info and combines the towers and hit info for easier analysis, with structure:

 - branch: vector of tower info for modules
 - branch: vector of location flags info for modules
 - branch: vector of IDHash info for modules
 - etc

## Pixel and SCT Map info

Information about the Pixel and SCT ROBIDs must be associated with each module. Dictionaries mapping the module id to the ROBID are already provided in the `config/generated` directory, but can be re-built with different inputs using `python/MappingParser.py` 

It is reconneded to build a cabling map from the df module configuration file using the function `buildIDHashROBMapfromModuleConfig`.

You can also build this map using the Pixel and SCT cabling maps from athena and an input IP file using the function `buildIDHashROBMap`

## Run DF Data Flow

To run the DF Data flow execute:

```
run_dfdataflow.py -c <configfilename> -n <# events> 
```

Existing config files live in the `config` directory. If no `-c` argument is passed, the code will use a default configuration file.

More options can be added by running:

```
 run_dfdataflow.py -c <configfilename> -i <inputfilename> -o <outputfiledir> -n <# events> -l <loglevel>
```
- `configfilename` should have the same structure as the [default config](https://gitlab.cern.ch/etolley/DFDataFlow/blob/master/config/default.config)
- `inputfilename` will override the input file in the config. This input file should be produced by `make_slimmed_ip_file.cxx`
- `outputfiledir` will provide a name of the output file directory. If nothing is specified this will be automatically generated.
- `loglevel` should be set to `DEBUG`, `INFO`, or `WARNING`

#### Code Structure

`config` directory contains Pixel and SCT cabling maps and FTK board configuration files

the pixel and SCT calbing maps are built using `python/MappingParser.py`


## Module & Tower Plots

To look at how many modules are going into which DFs:

`python plotting/plotModuleSummary.py`

To look at average dataflow in each board:

`python plotting/plotBoardStats.py`

