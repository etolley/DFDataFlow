#include <set>
#include <iostream>
#include <fstream>

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TString.h"
#include "TError.h" //gives us Info, Error, etc, methods to do printout

// Athena includes
#include "TrigFTKSim/FTKHit.h"
#include "TrigFTKSim/FTKRawHit.h"

ofstream myoutfile;

bool splitBranchesByTower = false;
bool saveWords = false;

// outTree branches
std::vector<int> n_modules(64);
std::vector<int> n_clusters(64);
std::vector<std::vector<int>*>  v_event(64);
std::vector<std::vector<int>*>  v_tower(64);
std::vector<std::vector<bool>*> v_module(64);
std::vector<std::vector<bool>*> v_isSCT(64);
std::vector<std::vector<bool>*> v_isPixel(64);
std::vector<std::vector<int>*>  v_BarrelEC(64);
std::vector<std::vector<int>*>  v_Layer(64);
std::vector<std::vector<int>*>  v_PhiModule(64);
std::vector<std::vector<int>*>  v_EtaModule(64);
std::vector<std::vector<int>*>  v_PhiSide(64);
std::vector<std::vector<int>*>  v_IDHash(64);
std::vector<std::vector<int>*>  v_moduleClusters(64);
std::vector<std::vector<int>*>  v_moduleSize(64);
std::vector<std::vector<int>*>  v_truncatedModuleSize(64);
std::vector<std::vector <std::vector<unsigned int>>*> v_moduleClusterWords(64);

void saveHit( const FTKRawHit * thisHit,
              int tower, int event,
              int nclusters,
              std::vector<unsigned int> clusterWords,
              bool splitBranchesByTower = true,
              bool verbose = false){

  // need to correct for the layer conversion
  // https://gitlab.cern.ch/atlas/athena/blob/21.3/Trigger/TrigFTK/TrigFTKSim/src/FTKRawHit.cxx#L279
  int layer = thisHit->getLayer();
  int etamodule = thisHit->getEtaModule();
  if (thisHit->getIsSCT()){
      if (thisHit->getBarrelEC() == 0){
        layer = layer/2;
      }
      else{
        layer = layer/4;
        if (etamodule > 0) etamodule = etamodule + 1;
      }
  }
  //if (layer >= 8) verbose = true;

  int moduleSize          = (std::min(64,nclusters) + 3)* 32;
  int truncatedModuleSize = (std::min(32,nclusters) + 3)* 32;

  if (verbose){
    std::cout  << "\t"  << splitBranchesByTower
               << " Tower: " << tower
               << " Event: " << event
               << " isSCT: "  << thisHit->getIsSCT()
               << " isPixel: "  << thisHit->getIsPixel()
               <<" BarrelEC: " << thisHit->getBarrelEC()
               << " layer: "  << layer
               //<<" Phi Module: " << thisHit->getPhiModule()
               //<<" Eta Module: " << etamodule
               //<< " Phi Side:" << thisHit->getPhiSide()
               << " ID Hash:" << thisHit->getIdentifierHash()
               << " # clusters: " << nclusters
               << std::endl;
  }

  int writeTower = tower;
  if (!splitBranchesByTower) tower = 0;

  v_event[tower]    ->push_back(event);
  v_tower[tower]    ->push_back(writeTower);
  v_module[tower]   ->push_back(1);
  v_isSCT[tower]    ->push_back(thisHit->getIsSCT());
  v_isPixel[tower]  ->push_back(thisHit->getIsPixel());
  v_BarrelEC[tower] ->push_back(thisHit->getBarrelEC());
  v_Layer[tower]    ->push_back(layer);
  v_PhiModule[tower]->push_back(thisHit->getPhiModule());
  v_EtaModule[tower]->push_back(etamodule);
  v_PhiSide[tower]  ->push_back(thisHit->getPhiSide());
  v_IDHash[tower]   ->push_back(thisHit->getIdentifierHash());
  v_moduleClusters[tower]->push_back(nclusters);
  v_moduleSize[tower]   ->push_back(moduleSize);
  v_truncatedModuleSize[tower]->push_back(truncatedModuleSize);
  if (saveWords) v_moduleClusterWords[tower]->push_back(clusterWords);

  myoutfile << " " << writeTower
            << " " << event
            << " "  << thisHit->getIsSCT()
            << " "  << thisHit->getIsPixel()
            <<" " << thisHit->getBarrelEC()
            << " "<< layer
            <<" " << thisHit->getPhiModule()
            <<" " << etamodule
            << " " << thisHit->getPhiSide()
            << " " << thisHit->getIdentifierHash();
  if (saveWords){
    for (auto w: clusterWords) myoutfile << " " << w;
  }
  myoutfile << "\n";
  

}

void clearTowerVectors(int tower){
  v_event[tower]    ->clear();
  v_tower[tower]    ->clear();
  v_module[tower]   ->clear();
  v_isSCT[tower]    ->clear();
  v_isSCT[tower]    ->clear();
  v_isPixel[tower]  ->clear();
  v_BarrelEC[tower] ->clear();
  v_Layer[tower]    ->clear();
  v_PhiModule[tower]->clear();
  v_EtaModule[tower]->clear();
  v_PhiSide[tower]  ->clear();
  v_IDHash[tower]   ->clear();
  v_moduleClusters[tower]->clear();

  if (saveWords){
    for (auto v: *v_moduleClusterWords[tower]) v.clear();
    v_moduleClusterWords[tower]->clear();
  }
  v_moduleSize[tower]    ->clear();
  v_truncatedModuleSize[tower]->clear();
  
}

void clearAllVectors(){
  for (int tower = 0; tower < 64; tower++){
    clearTowerVectors(tower);
  }
}

void initBranches(TTree * outTree, int tower, TString prefix){
    n_modules[tower]  = 0;
    n_clusters[tower] = 0;
    v_event[tower]    = 0;
    v_tower[tower]    = 0;
    v_module[tower]   = 0;
    v_isSCT[tower]    = 0;
    v_isPixel[tower]  = 0;
    v_BarrelEC[tower] = 0;
    v_Layer[tower]    = 0;
    v_PhiModule[tower]= 0;
    v_EtaModule[tower]= 0;
    v_PhiSide[tower]  = 0;
    v_IDHash[tower]   = 0;
    v_moduleClusters[tower] = 0;
    v_moduleClusterWords[tower] = 0;
    v_moduleSize[tower]     =0;
    v_truncatedModuleSize[tower] = 0;
    outTree->Branch(prefix+"nModulesPerEvent", &n_modules[tower]);
    outTree->Branch(prefix+"nClustersPerEvent", &n_clusters[tower]);
    outTree->Branch(prefix+"event", &v_event[tower]);
    outTree->Branch(prefix+"tower", &v_tower[tower]);
    outTree->Branch(prefix+"module", &v_module[tower]);
    outTree->Branch(prefix+"isSCT", &v_isSCT[tower]);
    outTree->Branch(prefix+"isPixel", &v_isPixel[tower]);
    outTree->Branch(prefix+"BEC", &v_BarrelEC[tower]);
    outTree->Branch(prefix+"layer", &v_Layer[tower]);
    outTree->Branch(prefix+"phiModule", &v_PhiModule[tower]);
    outTree->Branch(prefix+"etaModule", &v_EtaModule[tower]);
    outTree->Branch(prefix+"phiSide", &v_PhiSide[tower]);
    outTree->Branch(prefix+"IDHash", &v_IDHash[tower]);
    outTree->Branch(prefix+"moduleClusters", &v_moduleClusters[tower]);
    if (saveWords) outTree->Branch(prefix+"moduleClusterWords", &v_moduleClusterWords[tower]);
    outTree->Branch(prefix+"moduleSize", &v_moduleSize[tower]);
    outTree->Branch(prefix+"truncModuleSize", &v_truncatedModuleSize[tower]);
}


int make_slimmed_ip_file(  ) {

  splitBranchesByTower = false;
  saveWords = true;

  TString infilename = "/eos/atlas/user/j/jkuechle/public/ftk/ttbar.NTUP_FTKIP.root";
  //infilename = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/ipfiles/ftksim_64Towers_wrap.root";
  TString outfileTag = "NTUP_FTKIP_ttbar";

  infilename = "/eos/atlas/user/j/jkuechle/public/ftk/data18_13TeV.00360026.physics_EnhancedBias.NTUP_FTKIP.root";
  outfileTag = "NTUP_FTKIP_data18_13TeV.00360026.physics_EnhancedBias";

  infilename = "/eos/atlas/atlascerngroupdisk/det-ftk/tvlibrary/data18_pp_2017LowDF/2DF_358656_lb_300_ev1/IPGen/ftksim_64Towers_wrap.root";
  outfileTag = "NTUP_FTKIP_data18_pp_2017LowDF_filter";

  if(splitBranchesByTower) outfileTag += "_tower";
  if(saveWords)outfileTag += "_words";

  TFile * infile = new TFile(infilename,"READ");
  TTree * FTKHitTree = (TTree*)infile->Get("ftkhits");


  TFile * outfile = new TFile("data/" + outfileTag + ".root","RECREATE");
  myoutfile.open ("data/" + outfileTag + ".txt");
  TTree * outTree = new TTree("ftkhits", "slimmed ftk hit tree");

  myoutfile << "# Tower Event isSCT isPixel BarrelEC Layer PhiModule EtaModule PhiSide IDHash Words\n";


  if(splitBranchesByTower){
    for (int tower = 0; tower < 64; tower++){
      initBranches(outTree,tower, Form("Tower%d_",tower));
    }
  }else{
    initBranches(outTree,0,"");
  }


  std::vector<std::vector<FTKRawHit>*> RawHits(64);
  for (int tower = 0; tower < 64; tower++){
    RawHits[tower] = 0;
    auto hitb = FTKHitTree->GetBranch(Form("RawHits%d",tower));
    hitb->SetAddress(&RawHits[tower]);
  }

  for(int event = 0; event < FTKHitTree->GetEntries(); event++) {

    FTKHitTree->GetEntry(event);
    int avg_modules = 0;
    int avg_clusters = 0;
    
    // iterate over towers
    for (int tower = 0; tower < 64; tower++){
      if (RawHits[tower]->size() == 0) continue;
      std::set<unsigned int> sct_uniqueIds;
      std::set<unsigned int> pix_uniqueIds;
      map<unsigned int, FTKRawHit *> sct_uniqueHits;
      map<unsigned int, FTKRawHit *> pix_uniqueHits;
      map<unsigned int, int> sct_clusterCount;
      map<unsigned int, int> pix_clusterCount;
      map<unsigned int, std::vector<unsigned int>> sct_clusterWords;
      map<unsigned int, std::vector<unsigned int>> pix_clusterWords;
      //

      for (unsigned int  i = 0; i < RawHits[tower]->size(); i++){
        FTKRawHit * thisHit = &((RawHits[tower])->at(i));
        unsigned int idHash = thisHit->getIdentifierHash();  
        bool isSCT = thisHit->getIsSCT();

        if (thisHit->getIsSCT()){
          if (sct_uniqueIds.count(idHash) == 0){
            sct_uniqueIds.insert(idHash);
            sct_uniqueHits[idHash] = thisHit;
            sct_clusterCount[idHash] = 0;

          }
          sct_clusterCount[idHash] += 1;
          if (saveWords){
            sct_clusterWords[idHash].push_back(thisHit->getHWWord());
          }
        }else{
          if (pix_uniqueIds.count(idHash) == 0){
            pix_uniqueIds.insert(idHash);
            pix_uniqueHits[idHash] = thisHit;
            pix_clusterCount[idHash] = 0;

          }
          pix_clusterCount[idHash] += 1;
          if (saveWords){
            pix_clusterWords[idHash].push_back(thisHit->getHWWord());
          }
        }
      }
      for (auto idHash: sct_uniqueIds){
        saveHit(sct_uniqueHits[idHash],
                tower, event,
                sct_clusterCount[idHash], sct_clusterWords[idHash],
                splitBranchesByTower, false);
      }
      for (auto idHash: pix_uniqueIds){
        saveHit(pix_uniqueHits[idHash],
                tower, event,
                pix_clusterCount[idHash], pix_clusterWords[idHash],
                splitBranchesByTower, false);
      }
      //std::cout << ", # modules:" << uniqueIds.size() << std::endl;

      int towerIndex = splitBranchesByTower ? tower : 0;
      n_modules[towerIndex] = sct_uniqueIds.size() + pix_uniqueIds.size();
      n_clusters[towerIndex] = RawHits[tower]->size();
      avg_modules += n_modules[towerIndex];
      avg_clusters += n_clusters[towerIndex];
      
    }
    outTree->Fill();
    avg_modules /= 64.;
    avg_clusters /= 64.;
    std::cout << event << " -- avg # modules:" << avg_modules << ", avg # clusters:" << avg_clusters << std::endl;
    
    if (splitBranchesByTower) clearAllVectors();
    else clearTowerVectors(0);
  }

  outTree->Write();
  outfile->Close();
  infile->Close();

  myoutfile.close();

  return 0;
 
}

