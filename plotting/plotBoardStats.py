import ROOT

shelf_1 = range(0,30,4)
shelf_2 = [i+1 for i in range(0,30,4)]
shelf_3 = [i+2 for i in range(0,30,4)]
shelf_4 = [i+3 for i in range(0,30,4)]

all_boards = shelf_1 + shelf_2 + shelf_3 + shelf_4

#Board# WordsReceivedIM WordsReceivedFabric WordsReceivedFiber WordsSentTower WordsSentFabric WordsSentFiber
hWordsReceivedIM     = ROOT.TH1D("h1","h1",len(all_boards ), 0, 32)
hWordsReceivedFabric = ROOT.TH1D("h2","h2",len(all_boards ), 0, 32)
hWordsReceivedFiber  = ROOT.TH1D("h3","h3",len(all_boards ), 0, 32)
hWordsSentSSB      = ROOT.TH1D("h4a","h4a",len(all_boards ), 0, 32)
hWordsSentAUX      = ROOT.TH1D("h4b","h4b",len(all_boards ), 0, 32)
hWordsSentFabric     = ROOT.TH1D("h5","h5",len(all_boards ), 0, 32)
hWordsSentFiber      = ROOT.TH1D("h6","h6",len(all_boards ), 0, 32)

hist_list = [hWordsReceivedIM, hWordsReceivedFabric, hWordsReceivedFiber, hWordsSentSSB, hWordsSentAUX, hWordsSentFabric, hWordsSentFiber]
color_list = [ROOT.kAzure+5, ROOT.kRed-7, ROOT.kOrange-4,
              ROOT.kBlue+2 , ROOT.kBlue-7, ROOT.kPink+1, ROOT.kYellow-9]

drawWords = True

def loadBoardStats(infilename):
	infile = open(infilename, 'r')
	readingfile = drawWords
	for line in infile:
		print line
		if drawWords and 'Module' in line:
			break
		if not drawWords and 'Module' in line:
			readingfile = True
		if not readingfile:
			continue
		if line.strip()[0] == '#': continue
		vals = [t for t in line.strip().split()]
		print vals
		b = all_boards.index(int(vals[0])) + 1
		wordCounts = [float(v) for v in vals[1:]]
		hWordsReceivedIM.SetBinContent(b,wordCounts[0])
		hWordsReceivedFabric.SetBinContent(b,wordCounts[1])
		hWordsReceivedFiber.SetBinContent(b,wordCounts[2])
		hWordsSentSSB.SetBinContent(b,wordCounts[3])
		hWordsSentAUX.SetBinContent(b,wordCounts[4])
		hWordsSentFabric.SetBinContent(b,wordCounts[5])
		hWordsSentFiber.SetBinContent(b,wordCounts[6])
		

indir = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/output/"
fixedmap  = "DataFlow_NTUP_FTKIP_ttbar_words_30evts_32boards_fixedMap/boardSummary.txt"
randommap = "DataFlow_NTUP_FTKIP_ttbar_words_5evts_32boards_randomMap/boardSummary.txt"

loadBoardStats("{0}/{1}".format(indir, fixedmap ))

for i, h in enumerate(hist_list):
	h.SetLineColor(ROOT.kBlack)
	h.SetFillColor(color_list[i])
	for b in range(len(all_boards)):
		h.GetXaxis().SetBinLabel(b+1,"{0}".format(all_boards[b]));
		#h.GetXaxis().SetBinLabel(b+1,"%s"%(b));
	

#===== Plotting =====#
ROOT.gStyle.SetLegendFillColor(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptStat(0)
canvas = ROOT.TCanvas('c','c', 600, 1000)
pad1 = ROOT.TPad("pad1","This is pad1",0.0,0.51,1.0,1.0);
pad2 = ROOT.TPad("pad2","This is pad2",0.0,0.01,1.0,0.5);
legend1 = ROOT.TLegend(0.2,0.65,0.8,0.88)
legend2 = ROOT.TLegend(0.2,0.65,0.8,0.88)
pad1.Draw()
pad2.Draw()

pad1.cd()

hWordsReceivedFabric.Add(hWordsReceivedIM)
hWordsReceivedFiber.Add(hWordsReceivedFabric)
hWordsReceivedFiber.SetMinimum(0)
hWordsReceivedFiber.SetMaximum(hWordsReceivedFiber.GetMaximum()*2)
hWordsReceivedFiber.GetYaxis().SetTitle("Avg {0}/event".format('words' if drawWords else 'modules'))
hWordsReceivedFiber.GetXaxis().SetTitle("Board #")
hWordsReceivedFiber.Draw('h')
hWordsReceivedFabric.Draw('hsame')
hWordsReceivedIM.Draw('hsame')

legend1.AddEntry(hWordsReceivedFabric, "{0} received from fabric".format('words' if drawWords else 'modules'))
legend1.AddEntry(hWordsReceivedFiber,  "{0} received from fiber".format('words' if drawWords else 'modules'))
legend1.AddEntry(hWordsReceivedIM,     "{0} received from IM".format('words' if drawWords else 'modules'))
legend1.Draw()
ROOT.gPad.RedrawAxis()

pad2.cd()

hWordsSentAUX.Add(hWordsSentSSB)
hWordsSentFabric.Add(hWordsSentAUX)
hWordsSentFiber.Add(hWordsSentFabric)
hWordsSentFiber.SetMinimum(0)
hWordsSentFiber.SetMaximum(hWordsSentFiber.GetMaximum()*2)
hWordsSentFiber.GetYaxis().SetTitle("Avg {0}/event".format('words' if drawWords else 'modules'))
hWordsSentFiber.GetXaxis().SetTitle("Board #")
hWordsSentFiber.Draw('h')
hWordsSentFabric.Draw('hsame')
hWordsSentAUX.Draw('hsame')
hWordsSentSSB.Draw('hsame')
legend2.AddEntry(hWordsSentFabric, "{0} sent to fabric".format('words' if drawWords else 'modules'))
legend2.AddEntry(hWordsSentFiber,  "{0} sent to fiber".format('words' if drawWords else 'modules'))
legend2.AddEntry(hWordsSentSSB,  "{0} sent to SSB".format('words' if drawWords else 'modules'))
legend2.AddEntry(hWordsSentAUX,  "{0} sent to AUX".format('words' if drawWords else 'modules'))
legend2.Draw()
ROOT.gPad.RedrawAxis()

canvas.cd()
canvas.Modified()
canvas.Update()
canvas.SaveAs("boardStats.png")
raw_input("Wait...")