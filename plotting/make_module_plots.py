 

import sys,  getopt, os
import ROOT
import json
import utils

import modulereader as mr
from dfconfigparser import parseModuleListFile

def main(argv):

	#===== define hists =====#	
	h_SCT = ROOT.TH1F('h_SCT',  'h_SCT',  100, 0, 100)
	h_Pix = ROOT.TH1F('h_Pix',  'h_Pix',  100, 0, 100)
	hists = [h_SCT, h_Pix]
	h_dict = {}

	#===== define reader =====#
	inputfilename = "data/NTUP_FTKIP_ttbar_words.txt"
	outputfilename = "output/ModuleStats_ttbar.root"
	modulelist_configfile = "config/df/modulelist_Data2018_64T.txt"
	with open("config/generated/PixelIDHashROBMap_fromconfig.json") as json_file:  
		pixelmap = json.load(json_file)
	with open("config/generated/SCTIDHashROBMap_fromconfig.json") as json_file:  
		sctmap = json.load(json_file)
	(robid_modid_tower_map, robid_modid_plane_map) = parseModuleListFile( modulelist_configfile)

	moduleReader = mr.ModuleReader(inputfilename, pixelmap, sctmap, maxEvts = 100)
	moduleReader.robid_modid_plane_map = robid_modid_plane_map
	moduleReader.robid_modid_tower_map = robid_modid_tower_map

	#===== loop over root file =====#

	workdir = os.environ['TestArea']
	outdir = "{0}/plotting/".format(workdir)
	plot_exts = ['.png']

	for evt in range(0, 100):
		print "Event {0}".format(evt)
		moduleReader.readEvent()
		modules = moduleReader.getModules()
		for robid, mods in modules.iteritems():
			if robid not in h_dict: h_dict[robid] = {}
			for mid, m in mods.iteritems():
				#print robid, mid, m
				if mid not in h_dict[robid]:
					hname = "h_ROBID{0}_MODID{1}".format(utils.toHex(robid), mid)
					h_dict[robid][mid] = ROOT.TH1F(hname,  hname,  100, 0, 100)

				h_dict[robid][mid].Fill(len(m.words))
				if m.isPix: h_Pix.Fill(len(m.words))
				else: h_SCT.Fill(len(m.words))
		
	#===== Plotting =====#
	ROOT.gStyle.SetLegendFillColor(0)
	ROOT.gStyle.SetLegendBorderSize(0)
	ROOT.gStyle.SetOptTitle(0)
	ROOT.gStyle.SetOptStat(0)

	# hist formatting
	colors = [ROOT.kPink+9, ROOT.kRed-4, ROOT.kOrange+1,ROOT.kYellow, ROOT.kTeal+1, ROOT.kAzure+1, ROOT.kBlue-7, ROOT.kViolet]

	for h in hists:
		h.GetXaxis().SetTitle("# words")
		#hists['h_SCTlayer{0}'.format(i)].Scale(sf)
		h.SetLineColor(ROOT.kBlack)
	h_SCT.SetFillColor(ROOT.kAzure+1)
	h_Pix.SetFillColor(ROOT.kBlue-7)

	# canvas setup
	canvas1 = ROOT.TCanvas("c1","c1",600,600)

	canvas1.cd()
	#canvas1.Draw()
	#canvas1.Modified()
	#canvas1.Update()

	
	h_SCT.Draw()
	#canvas1.SaveAs(plotname)
	outfile = ROOT.TFile(outputfilename,"RECREATE")

	for h in hists: h.Write()
	for robid, mh in h_dict.iteritems():
		for mid, h in mh.iteritems():
			h.GetXaxis().SetTitle("# words")
			h.SetLineColor(ROOT.kBlack)
			if robid >= 0x200000: h.SetFillColor(ROOT.kAzure+1)
			else: h.SetFillColor(ROOT.kBlue-7)
			h.Write()

	outfile.Close()

	raw_input("Press enter...")

if __name__=='__main__':
	main(sys.argv[1:])

		
