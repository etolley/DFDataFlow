setupATLAS
lsetup python
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

asetup Athena,21.3.12

echo "exporting python path"
# python path
export PYTHONPATH=$PWD/python:$PWD/TrigFTKSimTools/integration/df-spy-parse/:$PYTHONPATH