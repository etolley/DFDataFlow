import utils
from sets import Set

class OutputHelper:
	def __init__(self, outfiledir):
		self.outfiledir = outfiledir
		self.summarypixmodules = {}
		self.summarysctmodules = {}
		self.resetModules()
		self.traceModule = None
		
	# expect tuple i the form of (robid, moduleid)
	# print out extra info for this module
	def setTraceModule(self, trace):
		self.traceModule = trace
		
	def tracing(self, module):
		if not self.traceModule: return False
		return module.robid == self.traceModule[0] and module.id == self.traceModule[1]


	def resetModules(self):
		self.immodules     = {}
		self.fabricmodules = {}
		self.fibermodules  = {}

		for b in range(32):
			self.fabricmodules[b] = [None]*20
			self.fibermodules[b]  = [None]*4
			self.immodules[b]     = [None]*16
		#self.summarymodules = {}

	def addModuleSummary(self, modules):
		for m in modules:
			if m.isPix:
				if m.id not in self.summarypixmodules: self.summarypixmodules[m.id] = []
				self.summarypixmodules[m.id].append(m)
			elif m.isSCT:
				if m.id not in self.summarysctmodules: self.summarysctmodules[m.id] = []
				self.summarysctmodules[m.id].append(m)
	def addInputModules(self, boardnumber, modules):
		for m in modules:
			if m.lastlink == 'intrashelf': self.addFabricModules(boardnumber,[m])
			elif m.lastlink == 'fiber': self.addFiberModules(boardnumber,[m])

	def addFabricModules(self, boardnumber, modules):
		#if boardnumber not in self.fabricmodules:
		#	self.fabricmodules[boardnumber] = [None]*20

		# TODO: fix fabric channel
		for i in range(20):
			inputs_in_lane = [m for m in modules if m.fromslot == i]
			self.fabricmodules[boardnumber][i] = inputs_in_lane

	def addFiberModules(self, boardnumber, modules):
		#if boardnumber not in self.fibermodules:
		#	self.fibermodules[boardnumber] = [None]*4

		# TODO: fix fabric channel
		for i in range(4):
			inputs_in_lane = [m for m in modules if m.fromshelf == i]
			self.fibermodules[boardnumber][i] = inputs_in_lane

	def addIMModules( self, boardnumber, im_lanes, modules):
		#if boardnumber not in self.immodules:
		#	self.immodules[boardnumber] = [None]*16

		for i, lane in enumerate(im_lanes):
			inputs_in_lane = [m for m in modules if m.robid == lane]
			self.immodules[boardnumber][i] = inputs_in_lane

	def makeOutFile(self, eventnum, doHex = False):
		thisoutfilename = "{0}_{1}.txt".format(self.outfilename, eventnum)
		print 'making outfile {0}'.format(thisoutfilename)
		myoutfile = open(thisoutfilename,'w')
		def printandwrite(line):
			#print line
			myoutfile.write("{0}\n".format(line))

		for b in range(32):
			if b not in self.immodules: continue
			printandwrite("##############################")
			printandwrite("## Entries for DF Board {:02d}".format(b))
			printandwrite('# IM Channels')
			for i in range(16):
				if not self.immodules[b][i]: continue
				printandwrite("# DF{:02d}-IM{:02d}".format(b,i))
				for m in self.immodules[b][i]:
					printandwrite('# Module {0}, Event {1}'.format(m.id, m.event))
					printandwrite("{0:#0{1}x}".format(m.event,10))
					for d in m.getData(doHex): printandwrite(d)
			printandwrite('# Fabric Channels')
			for i in range(20):
				if not self.fabricmodules[b][i]: continue
				printandwrite("# DF{:02d}-FAB{:02d}".format(b,i))
				for m in self.fabricmodules[b][i]:
					printandwrite('# Module {0}, Event {1}'.format(m.id, m.event))
					printandwrite("{0:#0{1}x}".format(m.event,10))
					for d in m.getData(doHex): printandwrite(d)
			printandwrite('# Inter-Crate Channels')
			for i in range(4):
				if not self.fibermodules[b][i]: continue
				printandwrite("# DF{:02d}-IC{:02d}".format(b,i))
				for m in self.fibermodules[b][i]:
					printandwrite('# Module {0}, Event {1}'.format(m.id, m.event))
					printandwrite("{0:#0{1}x}".format(m.event,10))
					for d in m.getData(doHex): printandwrite(d)
		myoutfile.close()

	def writeOutLanes(self, boardnum, getOutLane):
		thisoutfilename = "{0}/outlanes_board{1}.txt".format(self.outfiledir, boardnum)
		print 'making outfile {0}'.format(thisoutfilename)
		myoutfile = open(thisoutfilename,'w')
		
		#print line
		def printandwrite(line, lane = 0):

			if '\n' in line:
				lines = line.split('\n')
				for l in lines:
					myoutfile.write("[DF outspy ch{2}] Address: {1} - {0}\n".format(l,str(printandwrite.counter).zfill(4), lane))
					printandwrite.counter += 1
			else:
				myoutfile.write("[DF outspy ch{2}] Address: {1} - {0}\n".format(line,str(printandwrite.counter).zfill(4), lane))
				printandwrite.counter += 1

		#printandwrite.counter = 0

		trailertemplate="0xe0da0000\n{0}\n0x00000000\n0x00000000\n0xcd7673be\n0xe0f00000"

		headertemplate = "0xb0f00000\n0xff1234ff\n0x00055353\n{0}\n0x00000c3c\n0x00000084"

		# do channel output
		# add header and trailer
		#printandwrite("### Board {0} outlanes".format(boardnum))

		lane_module_map = {}
		for i in range(36): lane_module_map[i] = {}
		for modules in self.summarypixmodules.values() + self.summarysctmodules.values():
			for m in modules:
				if m.currentboard != boardnum: continue


				finaltowers = utils.flatten_list(m.getPathTowers())
				for t in finaltowers:
					(lane1, lane2) = getOutLane(m.plane, boardnum, t)
					print m.plane, lane1, lane2
					if m.event not in lane_module_map[lane1]: lane_module_map[lane1][m.event] = []
					if m.event not in lane_module_map[lane2]: lane_module_map[lane2][m.event] = []
					lane_module_map[lane1][m.event].append(m)
					lane_module_map[lane2][m.event].append(m)
		for lane in range(36):
			print lane
		#for lane, event_module_map in lane_module_map.iteritems():
			myoutfile.write('HHHHHHHHHH DF ch{0} HHHHHHHHHH\n'.format(lane))
			printandwrite.counter = 0
			for event, modules in  lane_module_map[lane].iteritems():
				print "Board {0} Lane {1} has {2} modules".format(boardnum, lane, len(modules))
				eventstr = "{0:#0{1}x}".format(m.event,10)
				printandwrite(headertemplate.format(eventstr), lane)
				mids = [m.id for m in modules]
				unique_mids = list(Set(mids))
				for m in modules:
					if m.id in unique_mids:
						unique_mids.remove(m.id)
					else:
						#print "Found module {0} more than once, skipping...".format(m.id)
						duplicates = [thism for thism in modules if thism.id == m.id ]
						thism = utils.reduce_identical_list(duplicates)
						print "Repeats {0} times".format(len(duplicates))
						continue
					#printandwrite('# Module {0} going to plane {1}'.format(m.id, m.plane))
					#print m.firstboard, m.fromboard, m.currentboard
					eventstr = "{0:#0{1}x}".format(m.event,10)
					#printandwrite(headertemplate.format(eventstr), lane)
					for d in m.getData(doHex=True): printandwrite(d, lane)
				printandwrite(trailertemplate.format(eventstr), lane)

		myoutfile.close()


	def computeModuleSummary(self, summarymodules):
		all_stats = []
		for mid, modules in summarymodules.iteritems():
			module_stats = {"type":[], "robid":[], 
		                    "inputim": [],
		                    "inputdf": [],
		                    "outputdf": [],
		                    "penultimatedf": [],
		                    "plane": [],
		                    "nwords": [],
		                    "towers": []}
			finaltowers = []
			nEvents = len(modules) 
			for m in modules:
				module_stats["type"].append(m.isPix)
				module_stats["robid"].append(m.robid)
				module_stats["inputim"].append(m.inputimlane)
				module_stats["inputdf"].append(m.firstboard)
				module_stats["outputdf"].append(m.currentboard)
				module_stats["penultimatedf"].append(m.fromboard)
				module_stats["plane"].append(m.plane)
				module_stats["nwords"].append(len(m.words))
				module_stats["towers"].append(m.towers)
				finaltowers+=m.getPathTowers()
			module_stats["id"] = mid
			module_stats["robid"] = utils.reduce_identical_list(module_stats["robid"])
			module_stats["type"]    = utils.reduce_identical_list(module_stats["type"])
			module_stats["inputim"] = utils.reduce_identical_list(module_stats["inputim"])
			module_stats["inputdf"] = utils.reduce_identical_list(module_stats["inputdf"])
			module_stats["plane"]   = utils.reduce_identical_list(module_stats["plane"])
			module_stats["towers"]   = utils.remove_duplicates(utils.flatten_list(module_stats["towers"]))
			finaltowers   = utils.remove_duplicates(utils.flatten_list(finaltowers))
			#print mid, module_stats["outputdf"], module_stats["towers"],
			module_stats["outputdf"] = utils.remove_duplicates(module_stats["outputdf"] )
			module_stats["penultimatedf"] = utils.remove_duplicates(module_stats["penultimatedf"] )
			if nEvents > 3 and sorted(finaltowers) != sorted(module_stats["towers"]):
				print nEvents
				print modules
				utils.error("Something wrong with towers, module {0} robid {1} should have gone to {2} but went to {3}\n{4}".format(module_stats["id"], module_stats["robid"],module_stats["towers"],finaltowers, module_stats))
			if len(module_stats["outputdf"])*2 < len(finaltowers):
				print module_stats["towers"], finaltowers
				utils.error("Something went wrong with module {0} robid {1} mapping: output to {2} boards but {3} towers\n{4}".format( module_stats["id"], module_stats["robid"], len(module_stats["outputdf"]), len(module_stats["towers"]),module_stats))
			if len(module_stats["outputdf"]) < len(module_stats["penultimatedf"]):
				utils.error("Something went wrong with module {0} robid {1} mapping: final and penultimate dfs not consistent\n{2}".format( module_stats["id"], module_stats["robid"], module_stats))
			all_stats.append(module_stats)
		return all_stats

	def writeModuleSummary(self, getOutputType = None):
		pix_stats = self.computeModuleSummary(self.summarypixmodules)
		sct_stats = self.computeModuleSummary(self.summarysctmodules)

		outfilename = "{0}/moduleSummary.txt".format(self.outfiledir)
		print "Writing module summary to {0}".format(outfilename)

		myoutfile = open(outfilename,'w')

		data = ["# Type;", "Mod. #;","ROBID;","In DF #;","In IM chan.;","Layer;","Mean words;","RMS Words;",
			    "Destination DFs;","Penultimate DFs;","Destination Towers;"]
	 	if getOutputType: data.append("FTK Destination")

		formatstr = '  {0[0]:<6}{0[1]:<8}{0[2]:<9}{0[3]:<9}{0[4]:<13}{0[5]:>7}{0[6]:>12}{0[7]:>11}{0[8]:>15}{0[9]:>15}{0[10]:>15}'
		if getOutputType: formatstr += '{0[11]:>15}'

		line = formatstr.format(data)
		myoutfile.write(line + '\n')
		for module_stats in pix_stats + sct_stats:
			data = [int(module_stats["type"]),
				  	module_stats["id"],
				  	module_stats["robid"],
				 	module_stats["inputdf"],
				  	module_stats["inputim"],
				  	module_stats["plane"],
				  	'{:.3f}'.format(utils.mean(module_stats["nwords"])),
				  	'{:.3f}'.format(utils.rms(module_stats["nwords"])),
				  	module_stats["outputdf"],
				  	module_stats["penultimatedf"],
				  	module_stats["towers"]
				  	]
			if getOutputType: data.append(getOutputType(module_stats["robid"], module_stats["plane"]))
			data = [str(d) + ';' for d in data]
			line = formatstr.format(data)
			#print line
			myoutfile.write(line+ '\n')
		myoutfile.close()
		
	def writeBoardSummary(self, boardDict, nEvents):
		outfilename = "{0}/boardSummary.txt".format(self.outfiledir)
		print "Writing board summary to {0}".format(outfilename)
		myoutfile = open(outfilename,'w')
		myoutfile.write("## Word DataFlow\n")
		myoutfile.write("# Board# WordsReceivedIM WordsReceivedFabric WordsReceivedFiber WordsSentSSB WordsSentAUX WordsSentFabric WordsSentFiber\n")
		for n, b in boardDict.iteritems():
			myoutfile.write("{0} {1} {2} {3} {4} {5} {6} {7}\n".format(n, b.words_received_im*1./nEvents, b.words_received_fabric*1./nEvents, b.words_received_fiber*1./nEvents,
																	  b.words_sent_ssb*1./nEvents, b.words_sent_aux*1./nEvents, b.words_sent_fabric*1./nEvents,     b.words_sent_fiber*1./nEvents))
		myoutfile.write("## Module DataFlow\n")
		myoutfile.write("# Board# ModulesReceivedIM ModulesReceivedFabric ModulesReceivedFiber ModulesSentSSB ModulesSentAUX ModulesSentFabric ModulesSentFiber\n")
		for n, b in boardDict.iteritems():
			myoutfile.write("{0} {1} {2} {3} {4} {5} {6} {7}\n".format(n, b.modules_received_im*1./nEvents, b.modules_received_fabric*1./nEvents, b.modules_received_fiber*1./nEvents,
																	  b.modules_sent_ssb*1./nEvents, b.modules_sent_aux*1./nEvents,  b.modules_sent_fabric*1./nEvents,     b.modules_sent_fiber*1./nEvents))
		
		myoutfile.close()
			
	
if __name__=='__main__':
	print 'outputhelper'