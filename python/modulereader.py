import dfboard as df
import utils 
import logging, time
import ROOT
class ModuleReader:
	def __init__(self, inputfilename, pixelmap, sctmap, maxEvts = 1000):
		self.event = 0
		self.prevEvent = 0
		self.maxEvts = maxEvts
		self.pixelmap = pixelmap
		self.sctmap = sctmap
		#self.moduleMap = {}
		if '.root'  in inputfilename:
			self.file = ROOT.TFile(inputfilename, 'READ')
			self.readEvent = self.readRootEvent
		
		elif '.txt' in inputfilename:
			self.file = open(inputfilename,'r')
			self.readEvent = self.readTxtEvent
			
		self.robid_modid_plane_map = None
		self.robid_modid_tower_map = None

	def getEvent(self):
		return self.event 
		
	def getModules(self):
		return self.moduleMap

	def readTxtEvent(self):
		self.moduleMap = {}
		for line in self.file:
			toks = line.strip().split()
			if len(toks) == 0: continue
			if '#' == toks[0]: continue
			(tower, self.event, isSCT, isPixel, barrel_ec, layer, phi_module, eta_module, phiside, idHash) = toks[:10]
			words   = toks[10:]
			self.event   = int(self.event)
			tower   = int(tower)
			isSCT   = isSCT == '1'
			isPixel = isPixel == '1'

			# things to process at the end of an event
			if self.event != self.prevEvent:
				if self.event!= 0:
					self.prevEvent = self.event
					return self.moduleMap

			self.prevEvent = self.event
			if self.event > self.maxEvts-1: break

			# build module
			module = self.defineModule(self.event, idHash, tower, layer, words, isSCT, isPixel, self.pixelmap, self.sctmap)

			# add module to map & check consistency with other modules
			if module.robid not in self.moduleMap:
				self.moduleMap[module.robid] = {}
			if module.id not in self.moduleMap[module.robid]:
				if self.robid_modid_tower_map:
					module.towers = self.robid_modid_tower_map[module.robid][module.id]
					module.towers_from_ipfile = [module.tower]
				else:
					module.towers = [module.tower]
				self.moduleMap[module.robid][module.id] = module
			else:
				if self.robid_modid_tower_map:
					self.moduleMap[module.robid][module.id].towers_from_ipfile.append(module.tower)
				else: self.moduleMap[module.robid][module.id].addTowers(module.towers)
				if self.moduleMap[module.robid][module.id].words != module.words:
					print '\nERROR! the same module mapped to different words in different towers'
					print mid, self.moduleMap[module.robid][module.id].towers
					print self.moduleMap[module.robid][module.id].words
					print module.words
					sys.exit(2)

	def readRootEvent(self):
		return 0
	'''
	# merge modules across different towers
	def compressIMModules(self, ROBmap):
		# build merged modules
		self.sct_modules = {}
		self.pix_modules = {}
		for mid, modules in self.moduleMap.iteritems():
			if len(modules) == 0: continue
			for module in modules:

				# crosscheck with module map
				if module.isSCT:
					if mid in self.sct_modules:
						self.sct_modules[mid].addTowers(module.towers)
						if self.sct_modules[mid].words != module.words:
							print '\nERROR! the same SCT module mapped to different words in different towers'
							print mid, self.sct_modules[mid].towers
							print self.sct_modules[mid].words
							print module.words
							sys.exit(2)
					else: self.sct_modules[mid] = module
				else:
					if mid in self.pix_modules:
						self.pix_modules[mid].addTowers(module.towers)
						if self.pix_modules[mid].words != module.words:
							print 'ERROR! the same Pixel module mapped to different words in different towers'
							print mid, self.pix_modules[mid].towers
							print self.pix_modules[mid].words
							print module.words
							sys.exit(2)
					else: self.pix_modules[mid] = module
		#return (pix_modules, sct_modules )

	def checkConsistency(self, ROBmap, traceModule = None):
		for robid, modid_tower_map in ROBmap.iteritems():
			for modid, expected_towers in modid_tower_map.iteritems():

				matching_pix_modules = [m for m in self.pix_modules.values() if m.robid == robid and m.id == modid]
				matching_sct_modules = [m for m in self.sct_modules.values() if m.robid == robid and m.id == modid]
				for m in matching_pix_modules + matching_sct_modules:
					if sorted(m.towers) != sorted(expected_towers):
						logging.warning("Module id: {0} and robid: {1} towers from ip file and towers from ROBMap do not match!!".format(m.id,m.robid))
						logging.debug("ip file: {0}".format(sorted(m.towers)))
						logging.debug("robmap file: {0}".format(sorted(expected_towers)))
						logging.info("Forcing module towers to match robmap")
						m.towers = expected_towers
					if traceModule and m.id == traceModule[1] and m.robid == traceModule[0] and m.event == traceModule[2]:
						utils.trace("Preparing to send module: {0}".format(m))
						utils.trace("This module goes to towers: {0}".format(expected_towers))

	def getPixAndSCTModules(self):
		return (self.pix_modules, self.sct_modules )
	'''
		
	def defineModule(self,event, idHash, tower, layer, words, isSCT, isPixel, pixelmap, sctmap ):
		#figure out ROBID
		ROBID = -1
		if isPixel:
			if idHash in pixelmap: ROBID = pixelmap[idHash]
			else:
				print "No PIX ROBID FOUND FOR {0} IN EVENT {1}".format(idHash, event)
				return None
		elif isSCT:
			if idHash in sctmap: ROBID = sctmap[idHash]
			else:
				print "NO SCT ROBID FOUND FOR {0} IN EVENT {1}".format(idHash, event)
				return None
		# send to board
		module = df.Module(thisid = int(idHash),
							 robid = ROBID,
							 tower = tower,
							 words = words,
							 layer = layer,
							 event = event)
		module.plane = utils.reduce_identical_list(self.robid_modid_plane_map[module.robid][module.id])
		return module

'''
	if '.root'  in inputfilename:
		runRootInput(inputfilename, boardDict, pixelmap, sctmap, ROBmap, myoutput,maxEvts = maxEvts)
	elif '.txt' in inputfilename:
		runTxtInput( inputfilename, boardDict, pixelmap, sctmap, ROBmap, myoutput,maxEvts = maxEvts)



def runTxtInput(inputfilename, boardDict, pixelmap, sctmap, ROBmap, output, maxEvts = 1):
	file = open(inputfilename,'r')

	timer = time.time()

	prevEvent = 0
	moduleMap = {}
	for line in file:
		toks = line.strip().split()
		if len(toks) == 0: continue
		if '#' == toks[0]: continue
		(tower, event, isSCT, isPixel, barrel_ec, layer, phi_module, eta_module, phiside, idHash) = toks[:10]
		words   = toks[10:]
		event   = int(event)
		tower   = int(tower)
		isSCT   = isSCT == '1'
		isPixel = isPixel == '1'

		# things to process at the end of an event
		if event != prevEvent:
			print "Event executed in {0}".format(time.time() - timer)
			timer = time.time()
			print '***********************************************'
			print '* Event {0}'.format(prevEvent)

			doDataFlow(prevEvent, boardDict, moduleMap, ROBmap, output)

			
			
			moduleMap = {}

		prevEvent = event
		if event > maxEvts-1: break

		# build module
		module = df.defineModule(event, idHash, tower, layer, words, isSCT, isPixel, pixelmap, sctmap)

		if int(idHash) not in moduleMap:
			moduleMap[int(idHash)] = []
		moduleMap[int(idHash)].append(module)

def runRootInput(inputfilename, boardDict, pixelmap, sctmap, ROBmap, output, maxEvts = 1):
	import ROOT
	inputfile = ROOT.TFile(inputfilename, 'READ')
	ftkhits = inputfile.Get("ftkhits")
	maxEvts = min(maxEvts, ftkhits.GetEntries())
	moduleMap = {}
	print "Parsing %s events..."%maxEvts
	for i in range(0, maxEvts):

		print 'Event', i

		# get event info from im
		ftkhits.GetEntry(i)
		#for tower in range (64):
		towers    = ftkhits.tower
		isSCTs    = ftkhits.isSCT
		isPixels  = ftkhits.isPixel
		idHashes  = ftkhits.IDHash
		wordSets  = ftkhits.moduleClusterWords

		# iterate over modules in this tower
		for j in range(len(idHashes)):
			tower = towers[j]
			(isSCT, isPixel) = (isSCTs[j], isPixels[j])
			idHash = "{0}".format(idHashes[j])
			words = [w for w in wordSets[i]]
			print words

			module = defineModule(event, idHash, tower, layer, words, isSCT, isPixel, pixelmap, sctmap)
			#for n, b in boardDict.iteritems():
			#	if ROBID in b.im_lanes: b.receive(module,'im')

			if int(idHash) not in moduleMap:
				moduleMap[int(idHash)] = []
			moduleMap[int(idHash)].append(module)

		print '***********************************************'
		print '* Event {0}'.format(i)
		doDataFlow(event, boardDict, moduleMap, ROBmap, output)
		output.clearAll()
		moduleMap = {}
'''