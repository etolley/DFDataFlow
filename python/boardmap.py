import dfboard as df
import utils
import time, sys, random, logging
from sets import Set

class BoardMap:
	def __init__(self, enabled_boards):
		self.enabled_boards = enabled_boards
		self.planeToOutBitMap = {}
		self.deterministicMap = False
		self.traceModule = None
	
	# expect tuple i the form of (robid, moduleid)
	# print out extra info for this module
	def setTraceModule(self, trace):
		self.traceModule = trace
		
	def tracing(self, module = None, robid = None, modid = None):
		if not self.traceModule: return False
		if module:
			return module.robid == self.traceModule[0] and module.id == self.traceModule[1] and module.event == self.traceModule[2]
		return robid == self.traceModule[0] and modid == self.traceModule[1]

	def printSummary(self):
		for n, b in self.boardDict.iteritems():
			b.getInfo()
	def getActiveTowers(self):
		self.active_towers = []
		for n, b in self.boardDict.iteritems():
			self.active_towers.append(b.tower1)
			self.active_towers.append(b.tower2)
		return self.active_towers

	def getBoard(self, shelf, slot):
		n = [i for i in self.boardDict.keys() if self.boardDict[i].shelf == shelf and self.boardDict[i].slot == slot]
		return n[0]

	def getOutputType(self, robid, plane):
		dettype = self.robid_dettype_map[robid]
		dest = 'AUX' if plane in [1, 2, 3, 4, 6, 8, 9, 10] else 'SSB'
		# cross check this with SLinkOut mapping
		outbit = self.planeToOutBitMap[plane]
		if (outbit == 8 and dest == 'AUX') or  (outbit != 8 and dest == 'SSB') :
			utils.error('Inconsistency in module output destination')
		return "{0}{1} {2}{3}".format(dest, outbit, dettype, plane)

	def getSLinkOutChannel(self, plane, boardnum, tower):
		outbit = self.planeToOutBitMap[plane]
		outlane = -1
		if tower == self.boardDict[boardnum].tower1:
			if outbit == 8: outlane = 16# SSB	
			else:		   outlane = outbit  #AUX 
		elif tower == self.boardDict[boardnum].tower2: 
			if outbit == 8: outlane = 17# SSB 
			else:		   outlane = outbit + 18  #AUX  
		else: return -1	

		AUXCH2PORT0SLINKOUTCH = [0,  1,  2,  3,  4,  5,  6,  7, 18, 19, 20, 21, 22, 23, 24, 25] #AUX_TOP0_BOT0
		AUXCH2PORT1SLINKOUTCH = [8,  9, 10, 11, 12, 13, 14, 15, 26, 27, 28, 29, 30, 31, 32, 33]

		SSBCH2PORT0SLINKOUTCH = [16, 17]
		SSBCH2PORT1SLINKOUTCH = [34, 35]

		if outlane == 17:   return (17,35)
		elif outlane == 16: return (16, 34)
		else: return (outlane, outlane + 8)

		return outlane

	def setBoards(self):
		self.boardDict = {}
		for n in self.enabled_boards:
			self.boardDict[n] =  df.Board(n)
			self.boardDict[n].setDict(self.boardDict)
			self.boardDict[n].planeToOutBitMap = self.planeToOutBitMap
			self.boardDict[n].setTraceModule(self.traceModule)
		if len(self.boardDict.keys()) == 0:
			logging.error("ERROR! No boards found! Check board config file")
			sys.exit(2)

	def setROBIDs(self, multiboard_configfile):
		boards_found = []
		self.input_robids = []
		self.robid_dettype_map = {}
		if '.json' in multiboard_configfile:
			import json
			with open( multiboard_configfile) as json_file:  
				# shelf-slot: [ ROB ID, cable label, IM channel #, detector type ]
				cablingmap = json.load(json_file)
			for shelfslot, cableinfo in cablingmap.iteritems():
				try:
					shelf, slot = [int(i) for i in shelfslot.split('-')]
				except:
					print shelfslot, cableinfo
					continue
				# get board corresponding to this shelf & slot
				n = self.getBoard(shelf, slot)
				boards_found.append(n)
				utils.log("Setting ROBIDS for Board {0} in shelf {1} slot {2}".format(n, shelf, slot))
				channel_mask, top_tower, bottom_tower = cableinfo[-3:]
				#if self.boardDict[n].tower1 != int(top_tower):
				#	print self.boardDict[n].tower1, self.boardDict[n].tower2, ' ' , top_tower, bottom_tower
				#	utils.error("ERROR! Board {0} top tower inconsistent between df system and cabling file".format(n))
				#if self.boardDict[n].tower2 != int(bottom_tower):
				#	print self.boardDict[n].tower1, self.boardDict[n].tower2, ' ', top_tower, bottom_tower
				#	utils.error("ERROR! Board {0} bottom tower inconsistent between df system and cabling file".format(n))
				for channel in cableinfo[:-3]:
					robid, label, imchan, dettype = channel
					robid = int(robid,16)
					self.robid_dettype_map[robid] = dettype
					if robid not in self.input_robids:
						self.input_robids.append( robid)
					else:
						#utils.error("ERROR! Robid {0} already sent to another board!".format(robid))
						print "ERROR! Robid {0} already sent to another board!".format( utils.toHex(robid))
					self.boardDict[n].setIMLaneROBID(int(imchan), robid)
					utils.log("Setting ROBID {0} in channel {1}, type: {2}".format(utils.toHex(robid), imchan, dettype))


		else:
			mfile = open(multiboard_configfile, 'r')
			for line in mfile:
				toks = line.strip().split()
				if len(toks)<3 or (toks[0][0] == '#'):
				  continue

				if toks[0] != "rodToBoard": continue
				if 'none' in line: continue

				robid  = int(toks[1], 16) # ROBId in hex
				dfnum  = int(toks[2])
				IMlane = int(toks[3])

				if dfnum not in boards_found: boards_found.append(dfnum)

				if dfnum in self.boardDict.keys():
					#boardmap[ dfnum ][ IMlane ] = robid
					self.boardDict[dfnum].setIMLaneROBID(IMlane, robid)

		if len(self.boardDict.keys()) == 0:
			logging.error("ERROR! No boards found! Check board config file")
			sys.exit(2)

		if sorted(boards_found) != sorted(self.boardDict.keys()):
			logging.error("ERROR! Not all boards found! Check board config file")

	def setConnections( self, insysfilename):
		insysfile = open(insysfilename, 'r')
		for line in insysfile:
			toks = line.strip().split()
			if "BoardNToShelfAndSlot" in line:
				boardnum = int(toks[1])
				shelf = int(toks[2])
				slot = int(toks[3])
				if boardnum in self.boardDict:
					self.boardDict[boardnum].shelf = shelf
					self.boardDict[boardnum].slot = slot
			elif "BoardNToTopTower" in line:
				if len(toks) < 3: continue
				boardnum = int(toks[1])
				if boardnum in self.boardDict:
					self.boardDict[boardnum].tower1 = int(toks[2])
			elif "BoardNToBotTower" in line:
				if len(toks) < 3: continue
				boardnum = int(toks[1])
				if boardnum in self.boardDict:
					self.boardDict[boardnum].tower2 = int(toks[2])
			elif "DFFiberConnection" in line:
				boardnum = int(toks[1])
				boardlink = int(toks[2])
				if boardnum in self.boardDict and boardlink in self.boardDict:
					self.boardDict[boardnum].fiberboard  =  self.boardDict[boardlink]
					self.boardDict[boardlink].fiberboard =  self.boardDict[boardnum]
			elif "PlaneToOutBit" in line:
				plane = int(toks[1])
				outbit = int(toks[2])
				self.planeToOutBitMap[plane] = outbit

	def setDesiredInputs(self, robid_modid_tower_map, robid_modid_plane_map):
		utils.log("Setting desired inputs...")
		self.ROBmap = robid_modid_tower_map
		self.planemap = robid_modid_plane_map
		self.all_desired_robids = []
		self.missing_robids = []
		for robid, modid_tower_map in robid_modid_tower_map.iteritems():
			if robid not in self.input_robids:
				utils.warning("ROBID {0} not set as an input!".format(utils.toHex(robid)))
				self.missing_robids.append(robid)
			if robid not in self.all_desired_robids:
				self.all_desired_robids.append(robid)
			for modid, towers in modid_tower_map.iteritems():
				for n,b in self.boardDict.iteritems():
					# board towers will receive module
					if b.tower1 in towers or b.tower2 in towers:
						
						if robid not in b.inputs:
							b.inputs[robid] = {}
						
						if robid in b.im_lanes:
							# receive this input directly from im
							b.inputs[robid][modid] = [df.IMLink( n, n, [ t for t in [b.tower1,b.tower2] if t in towers], 'im',  steps = 0)]
						
						else:
							# desired input, but unknown path
							b.inputs[robid][modid] = []
					
					if b.tower1 in towers:
						try:    b.tower1_robids[robid].append(modid) 
						except: b.tower1_robids[robid] = [modid]
					if b.tower2 in towers:
						try:    b.tower2_robids[robid].append(modid)
						except: b.tower2_robids[robid] = [modid]
		self.skip_robids = [r for r in self.input_robids if not r in self.all_desired_robids]
		utils.warning("ROBIDs {0} set as input but not needed by any towers".format( [utils.toHex(r) for r  in self.skip_robids]))

	def write(self, outputfiledir):
		pixmod2dst = {} # PIX module to destination board
		sctmod2dst = {} # SCT module to destination board
		pixmod2tower = {} # PIX module to FTK tower
		sctmod2tower = {} # SCT module to FTK tower
		pixmod2ftkplane = {} # PIX module to FTK plane
		sctmod2ftkplane = {} # SCT module to FTK plane

		for robid, modmap in self.ROBmap.iteritems():
			for mid, towers in modmap.iteritems():
				isSCT = robid >= 0x200000
				if isSCT: sctmod2tower[mid] = towers
				else:     pixmod2tower[mid] = towers

				for n, b in self.boardDict.iteritems():
					if b.tower1 in towers or b.tower1 in towers:
						if isSCT:
							if mid not in sctmod2dst: sctmod2dst[mid] = []
							sctmod2dst[mid].append(n)
						else:
							if mid not in pixmod2dst: pixmod2dst[mid] = []
							pixmod2dst[mid].append(n)

		for robid, modmap in self.planemap.iteritems():
			for mid, plane in modmap.iteritems():
				isSCT = robid >= 0x200000
				if isSCT: sctmod2ftkplane[mid] = plane
				else:     pixmod2ftkplane[mid] = plane

		import json
		
		with open("{0}/pixmod2dst.json".format(outputfiledir), 'w') as outfile:  
			json.dump(pixmod2dst, outfile, indent=4)
		with open("{0}/sctmod2dst.json".format(outputfiledir), 'w') as outfile:  
			json.dump(sctmod2dst, outfile, indent=4)
		with open("{0}/pixmod2tower.json".format(outputfiledir), 'w') as outfile:  
			json.dump(pixmod2tower, outfile, indent=4)
		with open("{0}/sctmod2tower.json".format(outputfiledir), 'w') as outfile:  
			json.dump(sctmod2tower, outfile, indent=4)
		with open("{0}/pixmod2ftkplane.json".format(outputfiledir), 'w') as outfile:  
			json.dump(pixmod2ftkplane, outfile, indent=4)
		with open("{0}/sctmod2ftkplane.json".format(outputfiledir), 'w') as outfile:  
			json.dump(sctmod2ftkplane, outfile, indent=4)

		with open("{0}/config.dump".format(outputfiledir), 'w') as outfile:  
			for mid, boards in pixmod2dst.iteritems():
				for b in boards:
					outfile.write("pixmod2dst\t{0}\t{1}\n".format(mid, b))
			for mid, boards in sctmod2dst.iteritems():
				for b in boards:
					outfile.write("sctmod2dst\t{0}\t{1}\n".format(mid, b))
			for mid, towers in pixmod2tower.iteritems():
				for t in towers:
					outfile.write("pixmod2tower\t{0}\t{1}\n".format(mid, t))
			for mid, towers in sctmod2tower.iteritems():
				for t in towers:
					outfile.write("psctmod2tower\t{0}\t{1}\n".format(mid, t))
			for mid, plane in pixmod2ftkplane.iteritems():
				outfile.write("pixmod2ftkplane\t{0}\t{1}\n".format(mid, plane))
			for mid, plane in sctmod2ftkplane.iteritems():
				outfile.write("sctmod2ftkplane\t{0}\t{1}\n".format(mid, plane))

		print "Wrote dictionaries and files to {0}".format(outputfiledir)



	def setInterBoardLinks(self):
		self.setAllPrimaryLinks()
		self.checkMissingLinks()
		self.setAllSecondaryAndTertiaryLinks(set2steplinks = True, set3steplinks = False)
		self.checkMissingLinks()
		self.setAllSecondaryAndTertiaryLinks(set2steplinks = False, set3steplinks = True)
		self.checkMissingLinks()
		
	def set_1step_links(self, b1, b2, linktype):
		for robid in b2.im_lanes:
			if robid == 0 or robid in self.skip_robids: continue
			try:
				for modid in self.ROBmap[robid].keys():
					outtower = []
					if b1.tower1WantsModule(robid, modid):
						outtower += [b1.tower1]
					if b1.tower2WantsModule(robid, modid):
						outtower += [b1.tower2]
					if len(outtower) > 0:
							b1_inlink =  df.InLink(  b1.board_number, b2.board_number, outtower, linktype, steps =1)
							b2_outlink = df.OutLink( b2.board_number, b1.board_number, outtower, linktype, steps =1)
							b1.addInputLink(robid, modid, b1_inlink)
							b2.addOutputLink(robid, modid, b2_outlink)
							if self.tracing(robid = robid, modid = modid):
								utils.trace("{0} {1} 1-step link {2}".format(robid, modid, b1_inlink))
								utils.trace("{0} {1} 1-step link {2}".format(robid, modid, b2_outlink))
			except:
				utils.error("ROBID {0} not in ROBmap".format(utils.toHex(robid)))

					
	def set_2step_link(self, b1, b2, bc, robid, linktype1, linktype2, modid = -1):
		outtower = []
		modids = []
		if b1.tower1WantsModule(robid, modid):
			outtower.append(b1.tower1)
			modids += b1.tower1_robids[robid]
		if b1.tower2WantsModule(robid, modid):
			outtower.append(b1.tower2)
			modids += b1.tower2_robids[robid]
			
		if modid != -1:
			modids = [modid]
		else:
			modids = list(Set(modids))

		if robid not in b1.inputs:  b1.inputs[robid]  = {}
		if robid not in b2.outputs: b2.outputs[robid] = {}
		if robid not in bc.inputs:  bc.inputs[robid]  = {}
		if robid not in bc.outputs: bc.outputs[robid] = {}

		inlink = df.InLink( b1.board_number, b2.board_number, outtower, [linktype1, linktype2], steps =2, middleboards = [bc.board_number])
		outlink = inlink.reverse()
		
		(inlinkpart, outlinkpart) = inlink.split(bc.board_number)
	 
		for m in modids:
			if self.tracing(robid = robid, modid = m):
				utils.trace("{0} {1} 2-step link {2}".format(robid, m, inlink))
				utils.trace("{0} {1} 2-step link {2}".format(robid, m, outlink))
				utils.trace("{0} {1} 2-step link (part) {2}".format(robid, m, inlinkpart))
				utils.trace("{0} {1} 2-step link (part) {2}".format(robid, m, outlinkpart))
			b1.addInputLink(robid, m, inlink)
			b2.addOutputLink(robid, m,outlink)
			bc.addInputLink(robid, m, inlinkpart )
			bc.addOutputLink(robid, m,outlinkpart)


	def set_3step_link(self, b1, b2, bc1, bc2, robid, modid = -1):
		outtower = []
		modids = []
		if robid in b1.tower1_robids.keys():
			outtower.append(b1.tower1)
			modids += b1.tower1_robids[robid]
		if robid in b1.tower2_robids.keys():
			outtower.append(b1.tower2)
			modids += b1.tower2_robids[robid]
		if modid != -1:
			modids = [modid]
		else:
			modids = list(Set(modids))
		
		if robid not in b1.inputs:   b1.inputs[robid]   = {}
		if robid not in b2.outputs:  b2.outputs[robid]  = {}
		if robid not in bc1.inputs:  bc1.inputs[robid]  = {}
		if robid not in bc1.outputs: bc1.outputs[robid] = {}
		if robid not in bc2.inputs:  bc2.inputs[robid]  = {}
		if robid not in bc2.outputs: bc2.outputs[robid] = {}

		inlink3  = df.InLink( b1.board_number, b2.board_number, outtower, ['intrashelf', 'fiber', 'intrashelf'], steps = 3, middleboards = [bc1.board_number, bc2.board_number])
		outlink3 = inlink3.reverse()
		links1 = inlink3.split(bc1.board_number)
		links2 = inlink3.split(bc2.board_number)

		logging.debug("Set Link from {0} to {1}".format(b1.board_number, b2.board_number))
		logging.debug("Set Link: {0}".format(inlink3))
		
		for m in modids:
			if self.tracing(robid = robid, modid = m):
				utils.trace("{0} {1} 3-step link {2}".format(robid, m, inlink3))
				utils.trace("{0} {1} 3-step link {2}".format(robid, m, outlink3))
				utils.trace("{0} {1} 3-step link (part) {2}".format(robid, m, links1))
				utils.trace("{0} {1} 3-step link (part) {2}".format(robid, m, links2))
			b1.addInputLink(robid, m, inlink3)
			b2.addOutputLink(robid, m,outlink3)
			bc1.addInputLink(robid, m, links1[0])
			bc1.addOutputLink(robid, m, links1[1] )

			bc2.addInputLink(robid, m, links2[0])
			bc2.addOutputLink(robid, m, links2[1])
		
	def setAllPrimaryLinks(self):
		logging.info("Setting 1-step links")
			# iterate over all boards
		for n1, b1 in self.boardDict.iteritems():
			for n2, b2 in self.boardDict.iteritems():
				if n1 == n2: continue
				if b1.shelf == b2.shelf:
					if b1 not in b2.shelfboardlist:
						b2.shelfboardlist.append(b1)
					if b2 not in b1.shelfboardlist:
						b1.shelfboardlist.append(b2)
					self.set_1step_links(b1, b2, 'intrashelf')
				elif b1.fiberboard != None and b1.fiberboard.board_number == n2:
					self.set_1step_links(b1, b2, 'fiber')

	def setAllSecondaryAndTertiaryLinks(self, set2steplinks = True, set3steplinks = False):
	
		random.seed(2)
		for b in [self.boardDict[n] for n in self.enabled_boards]: 

			#missing_robids = [r for r in b.inputs.keys() if len(b.inputs[r]) == 0]
			board_missing_inputs = b.tower1_missing_inputs + b.tower2_missing_inputs
			desired_board_links = list(Set([(robid, modid, rboard) for (robid, modid, rboard) in board_missing_inputs if rboard != -1]))

			logging.debug("================================================================")
			logging.info("Setting 2-{0}step links for board {1} to boards {2}".format(' and 3-' if set3steplinks else '', b.board_number, [i for __,__, i in desired_board_links]))

			for robid, modid, b2 in [(robid, modid, self.boardDict[n2]) for robid, modid,  n2 in desired_board_links]:
				foundLink = False
				logging.debug("----------------------------------------------------------------")
				logging.debug("Checking connection from board {0} to {1} for module {3} robid {2} ".format(b.board_number, b2.board_number,robid,modid ))
				if self.tracing(robid = robid, modid = modid): utils.trace("Determinding path for {0} {1} ".format(robid, modid))
				inshelf = b2.shelf
				logging.debug("Need to build link to shelf {0}".format(inshelf))
				# fiber has direct connection to shelf
				if b.fiberboard != None and b.fiberboard.shelf == inshelf:
					logging.debug("Board has direct fiber connection to desired shelf, set link!")
					if set2steplinks:
						self.set_2step_link(b, b2, b.fiberboard, robid, 'fiber', 'intrashelf''intrashelf', modid = modid)
					foundLink = True
					continue

				#possile intermediate boards for 3-step links
				possile_connections = []

				logging.debug("Checking shelf boards...")

				# another board in rack may have direct connection to shelf
				# check shelf board fiber connections
				for shelfb in b.shelfboardlist:
					if shelfb.fiberboard == None or shelfb.fiberboard.shelf != inshelf:
						continue
					
					# shelf board has direct fiber connection to board with robid
					if shelfb.fiberboard.board_number == b2.board_number:
						logging.debug("Shelf board has direct fiber connection to desired board!")
						if set2steplinks:
							self.set_2step_link(b, b2, shelfb, robid, 'intrashelf', 'fiber', modid = modid)
						foundLink = True
						break
					else:
						logging.debug("Shelf board {0} has desired fiber connection to shelf {1}".format(shelfb.board_number, inshelf))
						possile_connections+= [(shelfb.board_number, shelfb.fiberboard.board_number)]

				if not foundLink:
					if len(possile_connections) == 0:
						logging.warning("No connection possible for boards {0} and {1}".format(b.board_number, b2.board_number))
						continue
					else:
						logging.debug("Need to set 3-step connection between {0} and {1}".format(b.board_number, b2.board_number))
				if set3steplinks and not foundLink:
					if self.deterministicMap:
						possile_connections_nouts = [ len(self.boardDict[c1].outputs)+ len(self.boardDict[c2].outputs) for c1,c2 in possile_connections]
						logging.debug(possile_connections_nouts)
						index_smallest_nouts = possile_connections_nouts.index(min(possile_connections_nouts))
						logging.debug(index_smallest_nouts)
						pick_connection = possile_connections[index_smallest_nouts]
					else:
						pick_connection = random.choice(possile_connections)
					logging.info("Can Set 3-step link through for boards {0} and {1} through boards {2}, chose boards {3}".format(b.board_number, b2.board_number, possile_connections, pick_connection))
					self.set_3step_link(b, b2, self.boardDict[ pick_connection[0]], self.boardDict[pick_connection[1]], robid, modid = modid)

	def checkMissingLinks(self):
		self.towers_missing_inputs = {}
		for n, b in self.boardDict.iteritems():
			self.towers_missing_inputs[b.tower1] = {}
			self.towers_missing_inputs[b.tower2] = {}
			b.tower1_missing_inputs = []
			b.tower2_missing_inputs = []

			for robid, modid_linkmap in b.inputs.iteritems():
				for modid, links in modid_linkmap.iteritems():
					if len(links) != 0: continue
					if b.tower1WantsModule(robid,modid):
						if robid not in self.towers_missing_inputs[b.tower1]: self.towers_missing_inputs[b.tower1][robid] = []
						self.towers_missing_inputs[b.tower1][robid].append(modid)
						b.tower1_missing_inputs.append((robid, modid, -1))
					if b.tower2WantsModule(robid,modid):
						if robid not in self.towers_missing_inputs[b.tower2]: self.towers_missing_inputs[b.tower2][robid] = []
						self.towers_missing_inputs[b.tower2][robid].append(modid)
						b.tower2_missing_inputs.append((robid, modid, -1))

		for n1, b1 in self.boardDict.iteritems():
			for n2, b2 in self.boardDict.iteritems():
				if n1==n2: continue
				#print b1.tower1_missing_inputs
				for i, (robid, modid, n) in enumerate(b1.tower1_missing_inputs):
					if robid in b2.im_lanes: b1.tower1_missing_inputs[i] = (robid, modid,n2)
				for i, (robid, modid, n)   in enumerate(b1.tower2_missing_inputs):
					if robid in b2.im_lanes: b1.tower2_missing_inputs[i] = (robid, modid,n2)
		return self.towers_missing_inputs 
	# send IM modules to the different boards
	def receiveIMModules(self, module_map):
		
		valid_sct, valid_pix, bad_sct, bad_pix = 0, 0, 0, 0
		for robid, id_module_map in module_map.iteritems():
			if robid in self.skip_robids: continue
			for modid, module in id_module_map.iteritems():

				# crosscheck modules
				try:
					if sorted(module.towers_from_ipfile) != sorted(module.towers):
						utils.warning("Module id: {0} and robid: {1} towers from ip file {2} and towers from ROBMap {3} do not match!!".format(modid,robid, module.towers_from_ipfile, module.towers))
				except:
					pass
				module.towers = [t for t in module.towers if t in self.active_towers]
				module.towers = [t for t in module.towers if not module.robid in self.towers_missing_inputs[t]]

				module_sent = False

				for n, b in self.boardDict.iteritems():
						if module.robid in b.im_lanes:
							if module_sent:
								robidstr = "0x{:02x}".format(module.robid)
								print 'ERROR: Module {0} with ROBID {1} already sent to board...'.format(module.id, robidstr)
								sys.exit(2)

							module.inputimlane = b.im_lanes.index(module.robid)
							
							if self.tracing(module):
								utils.log("TRACING: Sending module from IM to board {0}: {1}".format(b.board_number, module))

							b.receive(module,'im')
							module_sent = True
				if not module_sent:
					utils.warning("Module id: {0} and robid: {1} not sent to board map!".format(modid,robid))


	def doDataFlow(self,output, event = 0, printSummary = False, savePerEventOutput = False):

		dataflowtime = time.time()
		# ido (IM), odo (Tower), ili, ilo
		step1 = {}
		step2 = {}
		step3 = {}

		# record IM inputs / buffer state before data flow
		for n, b in self.boardDict.iteritems(): step1[n] = b.getIO()

		utils.printtime("Pre-dataflow state recorded in {0} s".format(time.time() - dataflowtime))
		dataflowtime = time.time()

		# do all board-to-board data flow
		for n, b in self.boardDict.iteritems(): b.interBoardDataFlow()

		utils.printtime("Board-to-board data flow done in {0} s".format(time.time() - dataflowtime))
		dataflowtime = time.time()

		# pass modules from input buffers to output buffers
		for n, b in self.boardDict.iteritems():

			# record buffer state before data flow
			step2[n] = b.getIO()
			b.clearOutputs()
			
			# data flow
			b.intraBoardDataFlow()

			# record buffer state after data flow
			step3[n] = b.getIO()
			b.clearInputs()

		utils.printtime("Intraboard data flow done in {0} s".format(time.time() - dataflowtime))
		dataflowtime = time.time()

		# print summary
		for n, b in self.boardDict.iteritems():

			tower_modules_sent = step3[n][1]
			IM_modules_received = step1[n][0]
			interboard_modules_received = step2[n][2]
			interboard_modules_sent = step1[n][3]
			interboard_modules_willsend = step3[n][3]

			if savePerEventOutput:
				IM_modules_received = step1[n][0]
				output.addIMModules(n,b.im_lanes, IM_modules_received)
				output.addInputModules(n, interboard_modules_received)
				#output.addFiberModules(n,  fiber_modules_received)

			if printSummary:
			

				# figure out which modules are not being sent to board towers
				# or other boards
				IM_modules_not_sent = [m for m in step3[n][0] if m not in step3[n][1] and m not in step3[n][3]]
				interboard_modules_lost = [m for m in step3[n][2] if m not in step3[n][1] and m not in step3[n][3]]
				intrashelf_modules_received = [m for m in interboard_modules_received if m.lastlink == 'intrashelf']
				fiber_modules_received      = [m for m in interboard_modules_received if m.lastlink == 'fiber']
				tower1_modules_sent = [m for m in tower_modules_sent if b.tower1 in m.towers]
				tower2_modules_sent = [m for m in tower_modules_sent if b.tower2 in m.towers]

				def getModListStr(modlst):
					nmodules = len(modlst)
					nwords = sum([len(m.words) for m in modlst])
					return "{0} modules, {1} words".format(nmodules, nwords)

				#if printSummary:
				print '\n*** Board {0} ***'.format(n)
				print 'Received modules from IM:\t {0}'.format(getModListStr(IM_modules_received))
				print 'Received modules from shelf:\t {0}'.format(getModListStr(intrashelf_modules_received))
				print 'Received modules from fiber:\t {0}'.format(getModListStr(fiber_modules_received))

				print 'Will not send modules from IM:\t {0}'.format(len(IM_modules_not_sent))
				print 'Will not send interboard modules: {0}'.format(len(interboard_modules_lost))

				print 'Sent modules to other boards:\t {0}'.format(getModListStr(interboard_modules_sent))
				print 'Queued modules to other boards:\t {0}'.format(getModListStr(interboard_modules_willsend))
				print 'Unique tower outputs:\t\t {0}'.format(getModListStr(tower_modules_sent))
				print 'Send modules to tower {0}:\t {1}'.format(b.tower1, getModListStr(tower1_modules_sent))
				print 'Send modules to tower {0}:\t {1}'.format(b.tower2, getModListStr(tower2_modules_sent))

				print 'Total live inputs:\t\t {0}'.format(len(IM_modules_received + intrashelf_modules_received) - len(interboard_modules_lost + IM_modules_not_sent))
				print 'Total queued outputs:\t\t {0}'.format(len(interboard_modules_willsend) + len(tower_modules_sent))
			

			output.addModuleSummary(tower_modules_sent)


		utils.printtime("Post-processing done in {0} s".format(time.time() - dataflowtime))
		dataflowtime = time.time()

		if savePerEventOutput:
			output.makeOutFile(event, doHex = True)
			output.resetModules()



