# parse the DF module list file
# return a map containing all ROBIds -> tower number -> [ (modid, plane) ]
# where the list contains all modules coming from the ROB that are in the given tower
def layer_to_plane(layer, isPixel):
	'''
	SCT 0 2 4 5 6 go to AUX
	others go to SSB
	Pixel 1 2 3 go to AUX
	Pixel 0 to SSB
	'''
	if not isPixel:
		if	 layer == 0: return 4
		elif layer == 2: return 5
		elif layer == 4: return 6
		elif layer == 5: return 7
		elif layer == 6: return 8
		else:						return -1*layer

	return layer

def parseModuleListFile( fpname ):
	fp = open(fpname, 'r')
	ROBs = dict()
	robid_modid_tower_map = {}
	robid_modid_plane_map = {}

	for line in fp:
		toks = line.strip().split()
		if len(toks) < 4: continue

		robid = int(toks[0],16) # ROBId in hex
		modid = int(toks[1]) # modId in dec

		# add extra isSCT bit for sct mods
		#if robid >= 0x200000:
		#	modid |= (0x1 << 15)

		tow_0_31_bits = int(toks[2], 16) # tower map 0-31 in hex
		tow_32_63_bits = int(toks[3], 16) # "" 32-63 ""
		plane = int(toks[4])

		tow_all_bits = (tow_0_31_bits) | (tow_32_63_bits << 32)
		
		if robid not in robid_modid_tower_map:
			robid_modid_tower_map[robid] = {}
			robid_modid_plane_map[robid] = {}
		
		if modid not in robid_modid_tower_map[robid]:
			robid_modid_tower_map[robid][modid] = []
			robid_modid_plane_map[robid][modid] = []
		
		for itow in xrange(64):
			if ((0x1 << itow) & tow_all_bits):
				if itow not in robid_modid_tower_map[robid][modid]:
					robid_modid_tower_map[robid][modid].append(itow)
				robid_modid_plane_map[robid][modid].append(plane)	

		'''if robid not in ROBs:
			ROBs[robid] = {}

		for itow in xrange(64):
			if ((0x1 << itow) & tow_all_bits):
				if itow not in ROBs[robid].keys():
					ROBs[robid][itow] = []
				ROBs[robid][itow].append( ( modid, plane ) )'''

	return (robid_modid_tower_map, robid_modid_plane_map)
# end parseModuleListFile